# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polo', '0003_polocoin_currency_label'),
    ]

    operations = [
        migrations.AddField(
            model_name='polocoin',
            name='exchange_label',
            field=models.CharField(default=b'Poloniex', help_text=b'Polo coin exchange label', max_length=50),
        ),
    ]
