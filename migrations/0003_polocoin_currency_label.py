# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polo', '0002_auto_20170423_1824'),
    ]

    operations = [
        migrations.AddField(
            model_name='polocoin',
            name='currency_label',
            field=models.CharField(default=b'NONE', help_text=b'Polo coin currency label', max_length=10),
        ),
    ]
