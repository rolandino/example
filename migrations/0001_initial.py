# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PoloCoin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('polo_id', models.IntegerField(help_text=b'Polo coin ID', unique=True, db_index=True)),
                ('symbol', models.CharField(help_text=b'Polo coin symbol', unique=True, max_length=10, db_index=True)),
                ('name', models.CharField(help_text=b'Polo coin name', unique=True, max_length=200)),
                ('json_blob', models.TextField(help_text=b'JSON from Polo')),
                ('disabled', models.IntegerField(default=False, help_text=b'Disabled flag')),
                ('delisted', models.IntegerField(default=False, help_text=b'Delisted flag')),
                ('frozen', models.IntegerField(default=False, help_text=b'Frozen flag')),
                ('active', models.BooleanField(default=False, help_text=b'Indicate whether coin active or not on Polo', db_index=True)),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'Poloniex Coin',
                'verbose_name_plural': 'Poloniex Coins',
            },
        ),
    ]
