# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PoloData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('harvest_date_stamp', models.BigIntegerField(help_text=b'Harvest date as seconds since epoch', db_index=True)),
                ('harvest_date', models.DateTimeField(help_text=b'Date/time harvested from Polo', db_index=True)),
                ('json_blob', models.TextField(help_text=b'JSON from Polo')),
            ],
            options={
                'ordering': ['-harvest_date_stamp'],
                'verbose_name': 'Polo Data',
                'verbose_name_plural': 'Polo Data Items',
            },
        ),
        migrations.RemoveField(
            model_name='polocoin',
            name='json_blob',
        ),
        migrations.AddField(
            model_name='polodata',
            name='polo_coin',
            field=models.ForeignKey(help_text=b'Associated PoloCoin', to='polo.PoloCoin'),
        ),
    ]
