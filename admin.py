from django.contrib import admin

from .models import PoloCoin, PoloData


class PoloCoin_Admin(admin.ModelAdmin):

    search_fields = [
        'symbol',
        'name'
    ]
    list_filter = ('active',)

    list_display = (
        'polo_id',
        'symbol',
        'name',
        'active',
        'disabled',
        'delisted',
        'frozen',
        'currency_label',
        'exchange_label',
        'most_recent_cc_time'
    )
    ordering = ('symbol',)
    readonly_fields = (
        'polo_id',
        'symbol',
        'name',
        'active',
        'disabled',
        'delisted',
        'frozen',
        'currency_label',
        'exchange_label',
        'most_recent_cc_time'
    )

admin.site.register(PoloCoin, PoloCoin_Admin)


class PoloData_Admin(admin.ModelAdmin):

    list_filter = ('polo_coin',)

    list_display = (
        'polo_coin',
        'harvest_date',
        'json_blob',
    )
    ordering = ('-harvest_date_stamp',)
    readonly_fields = (
        'harvest_date_stamp',
        'harvest_date',
        'polo_coin',
        'json_blob',
    )

admin.site.register(PoloData, PoloData_Admin)
