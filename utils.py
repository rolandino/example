from django.forms.models import model_to_dict

from misc_utils.currency_mapping import Currency

from .models import PoloCoin


def get_polo_coins(active=False, property_param='id'):
    """
    Simple fn to return list of polo coins, ordered by
    a particular property.
    Also to return a dict, where the index is a particular
    property.
    """

    coin_instance_list = list(PoloCoin.objects.filter(active=active).order_by(
        property_param
    ))

    # Build a list with both values and the object itself:
    coins_list = []
    for coin_instance in coin_instance_list:
        coin_dict = model_to_dict(coin_instance)
        coin_dict['polo_instance'] = coin_instance
        coins_list.append(coin_dict)

    # Build a dict of coins, and add polo_url to each coin:
    coins_dict = {}
    for coin in coins_list:

        # Build and assign polo url:
        polo_url = polo_url_from_vars(
            exchange_label=coin['exchange_label'],
            currency_label=coin['currency_label'],
            symbol=coin['symbol']
        )
        coin['polo_url'] = polo_url

        # Build coin dict:
        coins_dict[
            coin[property_param]
        ] = coin

    return {
        'list': coins_list,
        'dict': coins_dict
    }


def get_polo_coins_list(active=False, property_param='id'):
    coins = get_polo_coins(active=active, property_param=property_param)
    return coins['list']


def get_polo_coins_dict(active=False, property_param='id'):
    coins = get_polo_coins(active=active, property_param=property_param)
    return coins['dict']


def polo_url(polo_coin):
    """
    Return direct URL to Poloniex for the currency.
    """
    return polo_url_from_vars(
        exchange_label=polo_coin.exchange_label,
        currency_label=polo_coin.currency_label,
        symbol=polo_coin.symbol
    )


def polo_url_from_vars(
    exchange_label,
    currency_label,
    symbol
):

    currency_string = Currency.get_currency_string(
        exchange=exchange_label,
        currency_label=currency_label
    )

    return "https://poloniex.com/exchange#%s_%s" % (
        currency_string,
        symbol.lower()
    )
