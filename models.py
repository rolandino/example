from __future__ import absolute_import

from calendar import timegm

from django.db import models
from django.utils import timezone

from misc_utils.currency_mapping import (
    Currency
)

from misc_utils.time_utils import (
    get_date_obj,
    get_epoch_time
)

from misc_utils.exchanges import (
    Exchange
)


class PoloCoin(models.Model):

    polo_id = models.IntegerField(
        help_text="Polo coin ID",
        unique=True,
        db_index=True
    )

    symbol = models.CharField(
        help_text="Polo coin symbol",
        unique=True,
        max_length=10,
        db_index=True
    )

    name = models.CharField(
        help_text="Polo coin name",
        unique=True,
        max_length=200
    )

    disabled = models.IntegerField(
        default=False,
        help_text="Disabled flag"
    )

    delisted = models.IntegerField(
        default=False,
        help_text="Delisted flag"
    )

    frozen = models.IntegerField(
        default=False,
        help_text="Frozen flag"
    )

    active = models.BooleanField(
        default=False,
        help_text="Indicate whether coin active or not on Polo",
        db_index=True
    )

    currency_label = models.CharField(
        help_text="Polo coin currency label",
        max_length=10,
        default=Currency.NONE_CURRENCY_LABEL
    )

    exchange_label = models.CharField(
        help_text="Polo coin exchange label",
        max_length=50,
        default=Exchange.POLONIEX
    )

    most_recent_cc_time = models.BigIntegerField(
        # Most recent crypto compare time value
        help_text="Most Recent Time from CryptoCompare",
        db_index=True,
        null=True
    )

    class Meta:
        ordering = ['name']
        verbose_name = "Poloniex Coin"
        verbose_name_plural = "Poloniex Coins"

    def __unicode__(self):
        return unicode(self.name)


class CustomPoloDataQuerySet(models.QuerySet):
    """
    Custom queryset for use in the PoloData model below.
    Custom update, and custom filter.
    """
    def update(self, **kwargs):
        """
        Custom update which ensures that harvest_date_stamp
        is updated with the current date/time - if it is
        not already defined in kwargs.
        """

        now = timezone.now()

        kwargs.setdefault(
            'harvest_date_stamp', timegm(
                now.timetuple()
            )
        )
        kwargs.setdefault(
            'harvest_date', now
        )
        return super(CustomPoloDataQuerySet, self).update(**kwargs)

    def harvest_date_stamp_filter(self, date_from_str, date_to_str):
        """
        Take two date/time strings
        find items that have harvest_date between and including
        those two harvest dates
        """

        timestamp_from = get_epoch_time(
            get_date_obj(date_from_str)
        )

        timestamp_to = get_epoch_time(
            get_date_obj(date_to_str)
        )

        return PoloData.objects.filter(
            harvest_date_stamp__gte=timestamp_from
        ).filter(
            harvest_date_stamp__lte=timestamp_to
        )


class CustomPoloDataManager(models.Manager):
    """
    Custom manager for CM model to enable harvest_date
    filter. And provide custom queryset
    that overrides update.
    """
    def get_queryset(self):
        return CustomPoloDataQuerySet(
            self.model, using=self._db
        )

    def harvest_date_stamp_filter(self, date_from_str, date_to_str):
        return self.get_queryset().harvest_date_stamp_filter(
            date_from_str, date_to_str
        )


class PoloData(models.Model):

    objects = CustomPoloDataManager()

    harvest_date_stamp = models.BigIntegerField(
        # Time since epoch version of harvest_date.
        # Want this as simple integer so that we can be
        # completely sure that PostGres uses the index when
        # doing a date-range query.
        help_text="Harvest date as seconds since epoch",
        db_index=True
    )

    harvest_date = models.DateTimeField(
        help_text="Date/time harvested from Polo",
        db_index=True
    )

    polo_coin = models.ForeignKey(
        PoloCoin,
        help_text="Associated PoloCoin"
    )

    json_blob = models.TextField(help_text="JSON from Polo")

    class Meta:
        ordering = ['-harvest_date_stamp']
        verbose_name = "Polo Data"
        verbose_name_plural = "Polo Data Items"

    def __unicode__(self):
        return unicode(self.harvest_date_stamp)
