import json
import requests
import time

from calendar import timegm

from django.db import transaction
from django.conf import settings
from django.core.mail import send_mail
from django.utils import timezone

from monitoring.models import CoinService
from monitoring.utils import set_service_message

from misc_utils.currency_mapping import Currency
from misc_utils.exchanges import Exchange
from misc_utils.json_utils import get_set_from_json

from ccompare.pull_cc import get_url

from .models import PoloCoin, PoloData
from .utils import polo_url


POLO_COIN_URL = "https://poloniex.com/public?command=returnCurrencies"
POLO_TICKER = "https://poloniex.com/public?command=returnTicker"


def query_url(url):
    """
    Query Polo API
    """
    response = requests.get(
        url
    )
    response_json = response.json()
    return response_json


def save_polo_data(
    polo_coin,
    coin_data
):
    # Save the coin data as PoloData:
    polo_data = PoloData()
    polo_data.json_blob = json.dumps(coin_data)

    harvest_date = timezone.now()
    harvest_date_stamp = timegm(harvest_date.timetuple())

    polo_data.harvest_date = harvest_date
    polo_data.harvest_date_stamp = harvest_date_stamp

    polo_data.polo_coin = polo_coin
    polo_data.save()


def save_polo_coin(
    polo_coin,
    coin_data,
    currency_label
):
    # Save coin:
    polo_coin.polo_id = coin_data['id']
    polo_coin.currency_label = currency_label
    polo_coin.name = coin_data['name']
    polo_coin.json_blob = json.dumps(coin_data)

    polo_coin.disabled = coin_data['disabled']
    polo_coin.delisted = coin_data['delisted']
    polo_coin.frozen = coin_data['frozen']

    # Check to see if active
    active = (
        polo_coin.disabled == 0 and
        polo_coin.delisted == 0 and
        polo_coin.frozen == 0 and
        currency_label != Currency.NONE_CURRENCY_LABEL
    )
    polo_coin.active = active
    polo_coin.save()

    # Save the polo data:
    save_polo_data(
        polo_coin,
        coin_data
    )


def get_currency_pair(currency_string, coin_symbol):
    return (
        "%s_%s" % (currency_string, coin_symbol)
    ).lower()


def get_currency_label(
    coin_symbol,
    currency_pairs,
    usd_currency_string,
    btc_currency_string,
    eth_currency_string
):

    btc_currency_pair = get_currency_pair(
        currency_string=btc_currency_string,
        coin_symbol=coin_symbol
    )

    usd_currency_pair = get_currency_pair(
        currency_string=usd_currency_string,
        coin_symbol=coin_symbol
    )

    eth_currency_pair = get_currency_pair(
        currency_string=eth_currency_string,
        coin_symbol=coin_symbol
    )

    currency_label = Currency.NONE_CURRENCY_LABEL

    # Check USD, then BTC, then ETH:
    if usd_currency_pair in currency_pairs:
        currency_label = Currency.get_currency_label(
            exchange=Exchange.POLONIEX,
            currency_string=usd_currency_string
        )

    elif btc_currency_pair in currency_pairs:
        currency_label = Currency.get_currency_label(
            exchange=Exchange.POLONIEX,
            currency_string=btc_currency_string
        )

    elif eth_currency_pair in currency_pairs:
        currency_label = Currency.get_currency_label(
            exchange=Exchange.POLONIEX,
            currency_string=eth_currency_string
        )

    if currency_label != Currency.NONE_CURRENCY_LABEL:
        dummy_coin = {}
        dummy_coin[
            'symbol'
        ] = coin_symbol
        dummy_coin[
            'currency_label'
        ] = currency_label
        dummy_coin[
            'exchange_label'
        ] = Exchange.POLONIEX

        test_url = get_url(
            coin=dummy_coin,
            nminutes=1
        )

        response_json = query_url(test_url)
        if response_json.get('Response', '') != 'Success':
            currency_label = Currency.NONE_CURRENCY_LABEL

    return currency_label


def process_coin_data(
    coin_symbol,
    coin_data,
    currency_pairs,
    usd_currency_string,
    btc_currency_string,
    eth_currency_string
):

    process_coin_msg = ""

    currency_label = get_currency_label(
        coin_symbol=coin_symbol,
        currency_pairs=currency_pairs,
        usd_currency_string=usd_currency_string,
        btc_currency_string=btc_currency_string,
        eth_currency_string=eth_currency_string
    )

    try:
        # Does one already exist?
        polo_coin = PoloCoin.objects.get(
            symbol=coin_symbol
        )

        # Yup, so, need to check status. Has anything changed?

        # Get the currently existing json, and the new json:
        polo_data_items = PoloData.objects.filter(polo_coin=polo_coin)
        if polo_data_items.count() == 0:
            # Define an empty set:
            existing_json_set = set({})
        else:
            existing_json = json.loads(
                polo_data_items[0].json_blob
            )
            existing_json[
                'currency_label'
            ] = str(polo_coin.currency_label)

            # Convert to set:
            existing_json_set = get_set_from_json(
                json_stuff=existing_json
            )

        # Get set for new json:
        new_json = coin_data
        new_json[
            'currency_label'
        ] = str(currency_label)
        new_json_set = get_set_from_json(
            json_stuff=new_json
        )

        # Have they changed?
        if (existing_json_set == new_json_set) is False:

            # Yes! So, what's changed:
            list_of_changes = list(
                new_json_set - existing_json_set
            )
            changes_summary = "\n".join(
                [str(change) for change in list_of_changes]
            )

            process_coin_msg = "Coin: symbol: %s, name: %s, has changed. url: %s\n.%s" % (
                coin_symbol,
                polo_coin.name,
                polo_url(polo_coin=polo_coin),
                changes_summary
            )

            # Resave coin:
            save_polo_coin(
                polo_coin=polo_coin,
                coin_data=coin_data,
                currency_label=currency_label
            )

    except PoloCoin.DoesNotExist:
        polo_coin = PoloCoin()
        polo_coin.symbol = coin_symbol

        # Save polo coin:
        save_polo_coin(
            polo_coin=polo_coin,
            coin_data=coin_data,
            currency_label=currency_label
        )

        # Flag that the coin is active, and pass the URL:
        if polo_coin.active is True:
            process_coin_msg = "New coin: symbol: %s, name: %s. Active. URL: %s" % (
                coin_symbol,
                polo_coin.name,
                polo_url(polo_coin=polo_coin)
            )
        else:
            process_coin_msg = "New coin: symbol: %s, name: %s. In-active." % (
                coin_symbol,
                polo_coin.name
            )

    return process_coin_msg


@transaction.atomic
def pull_from_polo():
    """
    Retrieve coin information from Poloniex.

    If coin exists, check whether status has changed.
    If coin does not exist, add.

    Keep track of data to record service message
    e.g. x new, y changed - record which ones,
    and how much data downloaded.

    But also keep track of new, changed - so can
    generate alert email.
    """

    start_time = time.time()

    # Get polo coins:
    coins_dict = query_url(POLO_COIN_URL)

    # Get polo ticker:
    polo_ticker = query_url(POLO_TICKER)
    currency_pairs = polo_ticker.keys()
    currency_pairs = [
        currency_pair.lower() for currency_pair
        in currency_pairs
    ]

    usd_currency_string = Currency.get_currency_string(
        exchange=Exchange.POLONIEX,
        currency_label=Currency.USD_CURRENCY_LABEL
    ).lower()

    btc_currency_string = Currency.get_currency_string(
        exchange=Exchange.POLONIEX,
        currency_label=Currency.BTC_CURRENCY_LABEL
    ).lower()

    eth_currency_string = Currency.get_currency_string(
        exchange=Exchange.POLONIEX,
        currency_label=Currency.ETH_CURRENCY_LABEL
    ).lower()

    # Lists to hold various messages:
    service_messages = []
    alert_messages = []

    # Get list of coin symbols:
    coin_symbols = coins_dict.keys()

    # Sort alphabetically:
    coin_symbols.sort()

    for coin_symbol in coin_symbols:
        # Get coin data from the dict:
        coin_data = coins_dict.get(
            coin_symbol,
            {}
        )

        # If there is valid coin data, process it:
        if coin_data:
            process_coin_msg = process_coin_data(
                coin_symbol=coin_symbol,
                coin_data=coin_data,
                currency_pairs=currency_pairs,
                usd_currency_string=usd_currency_string,
                btc_currency_string=btc_currency_string,
                eth_currency_string=eth_currency_string
            )

            # If there *is* a message, add it to
            # service messages and alert messages:
            if process_coin_msg:
                alert_messages.append(process_coin_msg)
                service_messages.append(process_coin_msg)

    # Record how many coins processed:
    service_messages.append(
        "Downloaded data from %i coins." % len(coin_symbols)
    )

    service_messages.append(
        "Storing data for %i coins." % PoloCoin.objects.count()
    )

    service_messages.append(
        "%i coins are active" % PoloCoin.objects.filter(active=True).count()
    )

    # Record time taken:
    time_taken_string = "Took: %.3f seconds" % (time.time() - start_time)
    service_messages.append(time_taken_string)

    service_message = "<br />".join(service_messages)
    set_service_message(
        service_id=CoinService.POLONIEX_CURRENCY_LIST,
        service_message=service_message
    )

    # If there are alert messages, make one message and send it via em:
    if alert_messages:
        alert_message = "\n".join(alert_messages)

        # send_mail: subject, message, from_email, recipient_list, fail_silently:
        email_subject = "Poloniex: Some Coins Changed their Status"
        email_message = alert_message
        send_mail(
            email_subject,
            email_message,
            settings.DEFAULT_FROM_ADDRESS,
            settings.POLO_EMAIL_ALERT_ADDRESSES,
            fail_silently=False
        )
