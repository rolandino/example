from __future__ import absolute_import

import textwrap

from django.core.management.base import BaseCommand
from polo.pull_polo import pull_from_polo


class Command(BaseCommand):
    help = textwrap.dedent("""
        Pull from Poloniex
        """)

    def handle(self, *args, **options):
        pull_from_polo()
